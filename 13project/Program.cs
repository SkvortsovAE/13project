﻿using _13project;

CustomLinkedList customLinkedList = new CustomLinkedList();

//Работаем с одним числом, предполагая что мы просто дублируем его получая красивое число,
//а затем ищем все варианты с помощью перестановок цифр
customLinkedList.Add(new Node());
customLinkedList.Add(new Node());
customLinkedList.Add(new Node());
customLinkedList.Add(new Node());
customLinkedList.Add(new Node());
customLinkedList.Add(new Node());

List<Counter> counters = new List<Counter>();

int calculatedFactorial = Factorial(customLinkedList.Count);

long result = 0; //количество всех красивых чисел для всех 13 значных 13 символьных чисел

while(true)
{
    ExploreNumberRepetitions();

    result += CalculateBeautifulNumbersForCurrentNumber();

    if (!customLinkedList.First().Decrement())
    {
        Console.WriteLine(result);
        break;
    }
}

int CalculateBeautifulNumbersForCurrentNumber()
{
    var permutations = calculatedFactorial / counters.Sum(x => Factorial(x.GetValue()));
    if (permutations == 1)
    {
        return permutations * 13; //случай для одинаковых цифр в числе
    }
    return (permutations + 1) * 13; //+1 - это исходное совпадение до перестановок.
                                    //*13 - учитываем все варианты с перестановками, с учетом того что исходное число задачи 13ти символьное (число символов нечетное)
                                    //и таким образом мы учитываем ту самую 13 цифру и получаем все комбинации
}

int Factorial(int n)
{
    if (n == 0) return 0;

    if (n == 1) return 1;

    return n * Factorial(n - 1);
}

void ExploreNumberRepetitions() //ищем и считаем количество вхождений для каждой цифры в чиисле
{
    foreach (Node node in customLinkedList)
    {
        var match = counters.FirstOrDefault(x => x.Number == node.Number);
        if (match != null)
        {
            match.IncreaseValue();
        }
        else
        {
            counters.Add(new Counter(node.Number));
        }
    }
}

