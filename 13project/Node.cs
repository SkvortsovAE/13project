﻿namespace _13project
{
    public class Node
    {
        public int Number { get; private set; } = 12;
        public Node? Next;

        public Node()
        {
            Next = null;
        }
        public Node(Node next)
        {
            Next = next;
        }

        private char NotationNumber
        {
            get
            {
                switch (Number)
                {
                    case 12:
                        return 'C';
                    case 11:
                        return 'B';
                    case 10:
                        return 'A';
                    case 9:
                        return '9';
                    case 8:
                        return '8';
                    case 7:
                        return '7';
                    case 6:
                        return '6';
                    case 5:
                        return '5';
                    case 4:
                        return '4';
                    case 3:
                        return '3';
                    case 2:
                        return '2';
                    case 1:
                        return '1';
                    case 0:
                        return '0';
                    default:
                        return '0';
                }
            }
        }

        public void DisplayNumber()
        {
            Console.Write(NotationNumber);
            if (Next != null)
            {
                Next.DisplayNumber();
            }
            else
            {
                Console.WriteLine();
            }
        }

        public bool Decrement()
        {
            if(Next == null && Number == 0)
            {
                return false;
            }

            var preCount = Number - 1;

            if (preCount < 0)
            {
                var result = Next.Decrement();
                if(result)
                {
                    Number = 12;
                }
                return result;
            }

            Number--;

            return true;
        }
    }
}
