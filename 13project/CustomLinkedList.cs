﻿using _13project;
using System.Collections;
using System.Collections.Generic;

public class CustomLinkedList : IEnumerable  // односвязный список
{
    Node? head; // головной/первый элемент
    Node? tail; // последний/хвостовой элемент
    int count;  // количество элементов в списке

    // добавление элемента
    public void Add(Node node)
    {
        if (head == null)
            head = node;
        else
            tail!.Next = node;
        tail = node;

        count++;
    }
    // удаление элемента
    public bool Remove(Node node)
    {
        Node? current = head;
        Node? previous = null;

        while (current != null)
        {
            if (current.Equals(node))
            {
                // Если узел в середине или в конце
                if (previous != null)
                {
                    // убираем узел current, теперь previous ссылается не на current, а на current.Next
                    previous.Next = current.Next;

                    // Если current.Next не установлен, значит узел последний,
                    // изменяем переменную tail
                    if (current.Next == null)
                        tail = previous;
                }
                else
                {
                    // если удаляется первый элемент
                    // переустанавливаем значение head
                    head = head?.Next;

                    // если после удаления список пуст, сбрасываем tail
                    if (head == null)
                        tail = null;
                }
                count--;
                return true;
            }

            previous = current;
            current = current.Next;
        }
        return false;
    }

    public Node First()
    {
        return head;
    }

    public int Count { get { return count; } }
    public bool IsEmpty { get { return count == 0; } }
    // очистка списка
    public void Clear()
    {
        head = null;
        tail = null;
        count = 0;
    }
    // содержит ли список элемент
    public bool Contains(Node node)
    {
        Node? current = head;
        while (current != null)
        {
            if (current.Equals(node)) return true;
            current = current.Next;
        }
        return false;
    }
    // добвление в начало
    public void AppendFirst(Node node)
    {
        node.Next = head;
        head = node;
        if (count == 0)
            tail = head;
        count++;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        Node? current = head;
        while (current != null)
        {
            yield return current;
            current = current.Next;
        }
    }
}