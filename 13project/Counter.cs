﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13project
{
    public class Counter
    {
        private int Value { get; set; }
        public int Number { get; private set; }
        public Counter(int number)
        {
            Number = number;
            Value = 1;
        }

        public void IncreaseValue()
        {
            Value++;
        }

        public int GetValue()
        {
            int cache = Value;
            Value = 0;
            return cache;
        }
    }
}
